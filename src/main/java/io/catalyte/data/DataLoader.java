package io.catalyte.data;


import io.catalyte.entities.Review;
import io.catalyte.entities.Vehicle;
import io.catalyte.repository.ReviewRepository;
import io.catalyte.repository.VehicleRepository;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;


@Component
public class DataLoader implements CommandLineRunner {

  private final Logger logger = LoggerFactory.getLogger(DataLoader.class);

  @Autowired
  private VehicleRepository vehicleRepository;
  @Autowired
  private ReviewRepository reviewRepository;

  private Vehicle vehicle1;
  private Vehicle vehicle2;
  private Vehicle vehicle3;
  private Vehicle vehicle4;
  private Vehicle vehicle5;
  private Vehicle vehicle6;
  private Vehicle vehicle7;
  private Vehicle vehicle8;

  private Review review1;
  private Review review2;
  private Review review3;
  private Review review4;
  private Review review5;
  private Review review6;

  @Override
  public void run(String... strings) throws Exception {
    logger.info("Loading data...");
    loadVehicles();
    loadReviews();
    logger.info("STRING!!!!!!!!!!!!!!!!!!!!!!!!");
  }

  private void loadVehicles() {
    vehicle1 = vehicleRepository.save(new Vehicle(1L, "SUV", "Kia", "Sorento", 2023));
    vehicle2 = vehicleRepository.save(new Vehicle(2L, "Hatchback", "Volkswagen", "Golf", 2015));
    vehicle3 = vehicleRepository.save(new Vehicle(3L, "Convertible", "Chevrolet", "Corvette", 2022));
    vehicle4 = vehicleRepository.save(new Vehicle(4L, "Sedan", "Honda", "Civic", 1999));
    vehicle5 = vehicleRepository.save(new Vehicle(5L, "Minivan", "Kia", "Sedona", 2023));
    vehicle6 = vehicleRepository.save(new Vehicle(6L, "SUV", "Honda", "Pilot", 2015));
    vehicle7 = vehicleRepository.save(new Vehicle(7L, "Sedan", "Honda", "Civic", 2007));
    vehicle8 = vehicleRepository.save(new Vehicle(8L, "Sedan", "Honda", "Civic", 2011));
  }

  private void loadReviews() {
    review1 = reviewRepository.save(new Review(1L, "Review1", "pertains to vehicleID 1", 5, new Date(), "user1", vehicle1));
    review2 = reviewRepository.save(new Review(2L, "Review2", "pertains to vehicleID 1", 4, new Date(), "user2", vehicle1));
    review3 = reviewRepository.save(new Review(3L, "Review3", "pertains to vehicleID 2", 5, new Date(), "user1", vehicle2));
    review4 = reviewRepository.save(new Review(4L, "Review4", "pertains to vehicleID 7", 5, new Date(), "user1", vehicle7));
    review5 = reviewRepository.save(new Review(5L, "Review5", "pertains to vehicleID 7", 4, new Date(), "user2", vehicle7));
    review6 = reviewRepository.save(new Review(6L, "Review6", "pertains to vehicleID 8", 5, new Date(), "user1", vehicle8));
  }
}