package io.catalyte.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Vehicle {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  private String type;
  private String make;
  private String model;
  private int year;

  @OneToMany(mappedBy = "vehicle", fetch = FetchType.EAGER)
  @JsonIgnore
  private Set<Review> reviews = new HashSet<>();

  public Vehicle() {
  }

  public Vehicle(Long id, String type, String make, String model, int year) {
    this.id = id;
    this.type = type;
    this.make = make;
    this.model = model;
    this.year = year;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getMake() {
    return make;
  }

  public void setMake(String make) {
    this.make = make;
  }

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  public int getYear() {
    return year;
  }

  public void setYear(int year) {
    this.year = year;
  }

  @Override
  public String toString() {
    return "Vehicle{" +
        "id=" + id +
        ", type='" + type + '\'' +
        ", make='" + make + '\'' +
        ", model='" + model + '\'' +
        ", year=" + year +
        '}';
  }
}
