package io.catalyte.entities;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Review {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  private String title;
  private String description;
  private int rating;
  private Date date;
  private String userName;
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn
  private Vehicle vehicle;

  public Review() {
  }

  public Review(Long id, String title, String description, int rating, Date date, String userName,
      Vehicle vehicle) {
    this.id = id;
    this.title = title;
    this.description = description;
    this.rating = rating;
    this.date = date;
    this.userName = userName;
    this.vehicle = vehicle;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public int getRating() {
    return rating;
  }

  public void setRating(int rating) {
    this.rating = rating;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public Vehicle getVehicle() {
    return vehicle;
  }

  public void setVehicle(Vehicle vehicle) {
    this.vehicle = vehicle;
  }

  //  public Long getVehicleId() {
//    return vehicleId;
//  }
//
//  public void setVehicleId(Long vehicleId) {
//    this.vehicleId = vehicleId;
//  }

  @Override
  public String toString() {
    return "Review{" +
        "id=" + id +
        ", title='" + title + '\'' +
        ", description='" + description + '\'' +
        ", rating=" + rating +
        ", date=" + date +
        ", userName='" + userName + '\'' +
        '}';
  }
}
