package io.catalyte.controllers;

import io.catalyte.entities.Vehicle;
import io.catalyte.repository.VehicleRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/vehicles")
public class VehicleController {

  @Autowired
  private VehicleRepository vehicleRepository;

  @GetMapping
  public ResponseEntity<List<Vehicle>> getVehicles() {
    return new ResponseEntity<>(vehicleRepository.findAll(), HttpStatus.OK);
  }

  @GetMapping("/{id}")
  public ResponseEntity<Vehicle> getVehicle(@PathVariable Long id) {
    Optional<Vehicle> vehicle = vehicleRepository.findById(id);

    if (vehicle.isPresent()) {
      return new ResponseEntity<>(vehicle.get(), HttpStatus.OK);
    } else {
      return null;
    }
  }

  @GetMapping("/search")
  public ResponseEntity<List<Vehicle>> findVehiclesByYear(
      @RequestParam("year") int year) {

    return new ResponseEntity<>(vehicleRepository.findByYear(year), HttpStatus.OK);
  }

  @PostMapping
  public ResponseEntity<Vehicle> addVehicle(@RequestBody Vehicle vehicle) {
    return new ResponseEntity<>(vehicleRepository.save(vehicle), HttpStatus.CREATED);
  }

  @PutMapping("/{id}")
  public ResponseEntity<Vehicle> replaceVehicle(
      @RequestBody Vehicle newVehicle, @PathVariable Long id) {

    boolean vehicleExists = vehicleRepository.existsById(id);

    if (vehicleExists) {
      return new ResponseEntity<>(vehicleRepository.save(newVehicle), HttpStatus.OK);
    } else {
      return null;
    }
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<Vehicle> deleteVehicle(@PathVariable Long id) {

    boolean vehicleExists = vehicleRepository.existsById(id);

    if (vehicleExists) {
      vehicleRepository.deleteById(id);
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } else {
      return null;
    }
  }

}