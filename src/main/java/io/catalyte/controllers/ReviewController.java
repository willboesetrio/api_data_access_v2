package io.catalyte.controllers;


import io.catalyte.entities.Review;
import io.catalyte.entities.Vehicle;
import io.catalyte.repository.ReviewRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/reviews")
public class ReviewController {

  @Autowired
  private ReviewRepository reviewRepository;
  @GetMapping
  public ResponseEntity<List<Review>> getEmployees() {
    return new ResponseEntity<>(reviewRepository.findAll(), HttpStatus.OK);
  }
  @GetMapping("/{id}")
  public ResponseEntity<Review> getReview(@PathVariable Long id) {
    Optional<Review> review = reviewRepository.findById(id);

    if (review.isPresent()) {
      return new ResponseEntity<>(review.get(), HttpStatus.OK);
    } else {
      return null;
    }
  }

  @GetMapping("/rating")
  public ResponseEntity<List<Review>> findReviewByRating(
      @RequestParam("rating") int rating) {

    return new ResponseEntity<>(reviewRepository.findByRating(rating), HttpStatus.OK);
  }

  @GetMapping("/search")
  public ResponseEntity<List<Review>> findReviewByMakeAndModel(
      @RequestParam("make") String make, @RequestParam("model") String model) {

    return new ResponseEntity<>(reviewRepository.findByVehicle_MakeAndVehicle_Model(make, model), HttpStatus.OK);
  }

  @GetMapping("/count")
  public ResponseEntity<Long> countReviewByMakeAndModel(
      @RequestParam("make") String make, @RequestParam("model") String model) {

    return new ResponseEntity<>(reviewRepository.countByVehicle_MakeAndVehicle_Model(make, model), HttpStatus.OK);
  }

  @PostMapping
  public ResponseEntity<Review> addReview(@RequestBody Review review) {
    return new ResponseEntity<>(reviewRepository.save(review), HttpStatus.CREATED);
  }

  @PutMapping("/{id}")
  public ResponseEntity<Review> ReplaceReview(
      @RequestBody Review newReview, @PathVariable Long id) {

    boolean reviewExists = reviewRepository.existsById(id);

    if (reviewExists) {
      return new ResponseEntity<>(reviewRepository.save(newReview), HttpStatus.OK);
    } else {
      return null;
    }
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<Review> deleteReview(@PathVariable Long id) {

    boolean reviewExists = reviewRepository.existsById(id);

    if (reviewExists) {
      reviewRepository.deleteById(id);
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } else {
      return null;
    }
  }

}