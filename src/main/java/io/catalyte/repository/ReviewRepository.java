package io.catalyte.repository;

import io.catalyte.entities.Review;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReviewRepository extends JpaRepository<Review, Long> {

  public List<Review> findByRating(int rating);

  public List<Review> findByVehicle_MakeAndVehicle_Model(String make, String model);

  public Long countByVehicle_MakeAndVehicle_Model(String make, String model);

}
