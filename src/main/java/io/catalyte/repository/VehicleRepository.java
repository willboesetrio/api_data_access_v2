package io.catalyte.repository;

import io.catalyte.entities.Vehicle;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VehicleRepository extends JpaRepository<Vehicle, Long> {


  List<Vehicle> findByYear(int year);
}

